const jwt = require('jsonwebtoken');
const config = require('../config');

const checkAuth = (req, res, next) => {
  try {
    const hasBearer = req.headers.authorisation.includes('Bearer');
    const authToken = req.headers.authorisation.split(' ')[1].length > 0 ? req.headers.authorisation.split(' ')[1] : false;

    if (hasBearer && authToken.length > 0){
      const decoded = jwt.verify(authToken, config.jwt_secret_key);
      req.AuthToken  = decoded;
      next();
    } else {
      res.status(401).json({message: 'Authorisation Failed'});
    }

  } catch(error) {
    res.status(401).json({message: 'Authorisation Failed'});
  }
}

module.exports = checkAuth;
