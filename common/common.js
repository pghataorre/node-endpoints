const returnResponseMessage = (req, res, statusCode, message) => {
  return res.status(statusCode).json({ message, status: statusCode});
}

const createDataEntry = (req, res, modelType, bodyObject) => {
  return modelType.create(bodyObject).then((response) => {
    return returnResponseMessage(req, res, 201, `Record created id ${response._id}`);
  }).catch((e) => {
    return returnResponseMessage(req, res, 500, e.message);
  });
}

const deleteDataEntry = async (req, res, modelType, id) => {
  try {
    const deleteResponse = await modelType.findByIdAndDelete({_id: id});
    return returnResponseMessage(req, res, 200, `${deleteResponse} Deleted`);

  } catch (e) {
    return returnResponseMessage(req, res, 500, e.message);
  }
}

module.exports = {
  returnResponseMessage,
  createDataEntry,
  deleteDataEntry,
}