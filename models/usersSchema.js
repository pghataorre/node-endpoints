const mogooose = require('mongoose');
const regexEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

const usersSchema = new mogooose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    match: regexEmail,
  },
  password: {
    type: String,
    required: true,
  },
  locked: {
    type: Boolean,
    required: true,
  }
});

module.exports = usersSchema;
