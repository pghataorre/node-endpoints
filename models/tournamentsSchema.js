const mongoose = require('mongoose');

const tournamentsSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
    unique: true
  },
  tournamentLogo: {
    type: String,
    require: true,
  },
  description: {
    type: String,
  },
  teams: [{ type : mongoose.Schema.Types.ObjectId,
      ref: 'teams',
      require: true,
    }]
  });

module.exports = tournamentsSchema;
