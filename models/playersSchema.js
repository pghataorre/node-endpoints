const mongoose = require('mongoose');

const playersSchema = new mongoose.Schema({
  firstName: {
    type: String,
    require: true,
  },
  lastName: {
    type: String,
    require: true,
  },
  shirtNumber: {
    type: String,
    required: true,
  },
  position: [{
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'positions',
    unique: true,
  }],
  teamId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'teams'
  },
  profileImage: {
    type: String,
  },
  profileDescription: {
    type: String,
  },
});

module.exports = playersSchema;
