const mongoose = require('mongoose');

const positionsSchema = new mongoose.Schema({
  positionKey: {
    type: String,
    require: true,
    unique: true,
  },
  position: {
    type: String,
    require: true,
    unique: true,
  }
});



// MAPPING REFERENCE
// { "position": "Striker", "positionKey": "STK" },
// { "position": "Attacking Midfielder", "positionKey": "AMID" },
// { "position": "Midfielder", "positionKey": "MID" },
// { "position": "Left Winger", "positionKey": "LW" },
// { "position": "Right Winger", "positionKey": "RW" },
// { "position": "Left back", "positionKey": "LB" },
// { "position": "Right Back", "positionKey": "RB" },
// { "position": "Center Back", "positionKey": "CB" },
// { "position": "Goal Keeper", "positionKey": "GK" }