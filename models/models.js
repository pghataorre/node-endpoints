const mongoose = require('mongoose');
const teamsSchema = require('./teamsSchema');
const managersSchema = require('./managerSchema');
const usersSchema = require('./usersSchema');
const tournamentsSchema = require('./tournamentsSchema');

const createModel = (collectionName, collectionSchema) => {
  try {
    return mongoose.model(collectionName);
  } catch (e) {
    return mongoose.model(collectionName, collectionSchema);
  }
}

module.exports = {
  teamsModel: createModel('teams', teamsSchema), 
  managersModel: createModel('managers', managersSchema),
  usersModel: createModel('users', usersSchema),
  tournamentsModel: createModel('tournaments', tournamentsSchema),
}