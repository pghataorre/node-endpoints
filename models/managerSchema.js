const mongoose = require('mongoose');

const managersSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
    unique: true
  },
});

module.exports = managersSchema;
