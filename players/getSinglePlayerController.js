 const mongoose = require('mongoose');
 const playersSchema = require('../models/playersSchema');
 const teamsSchema = require('../models/teamsSchema');
 const positionsSchema = require('../models/positionsSchema');
 const config = require('../config');
 const player = require('./players');

 const { returnResponseMessage } = require('../common/common');

const getSinglePlayer = async (req, res, playerId) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let playerModel, teamsModel, positionsModel, singlePlayerData;

  try {
    playerModel = mongoose.model('players');
  } catch(e) {
    playerModel = mongoose.model('players', playersSchema);
  }

  try {
    teamsModel = mongoose.model('teams');
  } catch(e) {
    teamsModel = mongoose.model('teams', teamsSchema);
  }

  try {
    positionsModel = mongoose.model('positions');
  } catch(e) {
    positionsModel = mongoose.model('positions', positionsSchema);
  }

  try {
    singlePlayerData = await playerModel.find({_id: playerId }).populate('teamId').populate('position');

    return {
      ...player,
      data: singlePlayerData
    }
  } catch(e) {
    return returnResponseMessage(req, res, 500, e.message);
  }
};

module.exports =getSinglePlayer;
