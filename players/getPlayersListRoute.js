const express = require('express');
const router = express.Router();
const getPlayersList = require('./getPlayersListController');

router.get('/teamplayers/:id',async (req, res, next) => {
  const teamId = req.id
  const playersCollection = await getPlayersList(req, res, teamId);
  res.json(playersCollection);
});

module.exports = router;
