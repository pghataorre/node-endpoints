const express = require('express');
const router = express.Router();
const { deleteSinglePlayer } = require('./deleteSinglePlayerController');

router.delete('/player/delete/:id', async (req, res, next) => {
  const playerId = req.id;
  const singleTeamToDelete = await deleteSinglePlayer(req, res, playerId);

  res.json(singleTeamToDelete);
});

module.exports = router;