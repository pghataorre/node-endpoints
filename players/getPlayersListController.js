const mongoose = require('mongoose');
const { players } = require('./players');
const playersSchema = require('../models/playersSchema');
const config = require('../config');
const { returnResponseMessage } = require('../common/common');

const getPlayersList = async (req, res, teamId) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let playersModel, playersCollection;

  // try{
  //   teamsModel = mongoose.model('teams');
  // } catch(e) {
  //   teamsModel = mongoose.model('teams', teamsSchema);
  // }

  try{
    playersModel = mongoose.model('players');
  } catch(e) {
    playersModel = mongoose.model('players', playersSchema);
  }

  try {
    playersCollection = await playersModel.find({teamId: teamId});
    return {
      ...players,
      data: playersCollection
    };

  } catch(e) {
    return returnResponseMessage(req, res, 500, e.message);
  }
}

module.exports = getPlayersList;
