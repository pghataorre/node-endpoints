const mongoose = require('mongoose');
const teamsSchema = require('../models/teamsSchema');
const config = require('../config');
const { deleteDataEntry } = require('../common/common');

const deleteSinglePlayer = async (req, res, teamId) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let playersModel, deleteResponse;

  try {
    playersModel = mongoose.model('players');
  } catch (error) {
    playersModel = mongoose.model('players', teamsSchema);
  }

  deleteResponse = deleteDataEntry(req, res, playersModel, teamId)
  return deleteResponse;
}

module.exports = {
  deleteSinglePlayer
};
