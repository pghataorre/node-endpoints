const express = require('express');
const router = express.Router();
const getSinglePlayer = require('./getSinglePlayerController');

 router.get('/player/:id', async (req, res) => {
    const playerId = req.id;
    const playerData = await getSinglePlayer(req, res, playerId);

    res.json(playerData);
 });

module.exports = router;