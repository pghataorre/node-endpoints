const mongoose = require('mongoose');
const { tournamentsModel } = require('../models/models');
const config = require('../config');

const getTournamentsList = async (req, res) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let tournamentsCollection;

  try {
    tournamentsCollection = await tournamentsModel.find({});
    res.status(200).json(tournamentsCollection);

  } catch(error) {
    res.status(500).json(error);
  };

  mongoose.disconnect();
}

const getSingleTournament = async (req, res) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let singleTournament;

  try {
    singleTourmament = await tournamentsModel.findById(req.params.id);
    res.status(200).json(singleTourmament);    
  } catch(error){
    res.status(500).json(error);
  }

  mongoose.disconnect();
}

const createTournament = async (req, res) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let tournamentCreate, teamsArray;

  try {
    req.body.teams = teamsArray = req.body.teams.split(',');

    tournamentCreate = await tournamentsModel.create(req.body);
    res.status(200).json(tournamentCreate);    
  } catch(error){
    res.status(500).json(error);
  }
  
  mongoose.disconnect();
}

const updateTournament = async (req, res) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let tournamentEntryUpdate;

  try {
    req.body.teams = req.body.teams.split(',');

    tournamentEntryUpdate = await tournamentsModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: req.body.teams },
      { new: true, useFindAndModify: false }
    );
    res.status(201).json(tournamentEntryUpdate);    
  } catch(error){
    console.log(error)
    res.status(500).json(error);
  }

  mongoose.disconnect();
};

module.exports = {
  getTournamentsList,
  getSingleTournament,
  createTournament,
  updateTournament
};
