const tournaments = {
  "desc": "Football Tournaments",
  "response": "200 application/json",
  "data": []
};

const singleTournament = {
  "desc": "Singular Football Tournaments",
  "response": "200 application/json",
  "data": []
};

module.exports = {
  tournaments,
  singleTournament
};
