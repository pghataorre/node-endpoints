const express = require('express');
const router = express.Router();
const checkAuth = require('../common/ checkAuth');
const {
  getTournamentsList,
  getSingleTournament,
  createTournament,
  updateTournament
} = require('./tournamentsController');

router.get('/', async (req, res) => {
  await getTournamentsList(req, res);
});

router.get('/:id', async(req, res) => {
  await getSingleTournament(req, res);
});

router.post('/', checkAuth, async(req, res) => {
  await createTournament(req, res);
});

router.put('/:id', checkAuth, async(req, res) => {
  await updateTournament(req, res);
});


module.exports = router;
