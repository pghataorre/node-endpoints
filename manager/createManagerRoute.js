const express = require('express');
const router = express.Router();
const checkAuth = require('../common/ checkAuth');

const createManager = require('./createManagerController');
const { returnResponseMessage } = require('../common/common');

router.post('/manager/create', checkAuth, (req, res, next) => {
  const managerData = req.body; 
  const { name } = managerData;

  if ((!name) && name === undefined) {
    return returnResponseMessage(req, res, 404, 'Post Data Error');
  }

  return createManager(req, res, managerData);
});

module.exports = router;