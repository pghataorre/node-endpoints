const mongoose = require('mongoose');
const { createDataEntry } = require('../common/common');
const managersSchema = require('../models/managerSchema');
const config = require('../config');

const createManager = (req, res, managerData) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let managerModel;

  try {
    managerModel = mongoose.model('managers');
  } catch (error) {
    managerModel = mongoose.model('managers', managersSchema);
  }

  return createDataEntry(req, res, managerModel, managerData);
}

module.exports = createManager;
