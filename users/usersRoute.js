const express = require('express');
const router = express.Router();
const { createUser, loginUser } = require('./usersController');

router.post('/register', async (req, res) => {
  await createUser(req, res);
});

router.post('/login', async (req, res) => {
  await loginUser(req, res);
});


module.exports = router;

