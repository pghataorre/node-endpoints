const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { usersModel } = require('../models/models');
const config = require('../config');
const saltRounds = 10;

const createUser = async (req, res) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let hashPwd;

  try {
    hashPwd = await bcrypt.hash(req.body.password, saltRounds);
    req.body.password = hashPwd;
    req.body.locked = false;

  } catch(error) {
    res.status(500).json(error);
  }

  try {
    const modelResponse = await usersModel.create(req.body);
    res.status(201).json(modelResponse);
  } catch(error) {
    res.status(500).json(error);
  }

  mongoose.disconnect();
}

const loginUser = async (req, res) => {
  mongoose.connect(config.mongooseUrl, config.mongooseParams);
  let usersCollection, enteredPassword, hasMatched;

  try {
    enteredPassword = req.body.password;
    usersCollection = await usersModel.findOne({email: req.body.email});

    if (!usersCollection) {
      res.status(401).json({message: 'not authenticated'});
    }

    hasMatched = await bcrypt.compare(enteredPassword, usersCollection.password);

    if (hasMatched) {
      const token = jwt.sign(
        req.body.email, 
        config.jwt_secret_key,
      )
      res.status(200).json({message: 'authenticated', jwt: token});
    } else {
      res.status(401).json({message: 'not authenticated'});
    }

  } catch(error) {
    console.log(error)
    res.status(401).json(error);
  } 

  mongoose.disconnect();
}

module.exports = { 
  createUser,
  loginUser
};
