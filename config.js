 var _ = require('lodash');
 
 var config = {
   dev: 'development',
   test: 'testing',
   prod: 'prod',
   port: process.env.PORT || 3000,
   mongooseUrl: 'mongodb://root:example@localhost/teams?authSource=admin&w=1',
   mongooseParams: { useNewUrlParser: true, useUnifiedTopology: true },
   jwt_secret_key: 'Ars3N4l'
 }

 process.env.NODE_ENV || config.dev;
 config.dev = process.env.NODE.ENV;
 
 var envConfig;

 try {
  envConfig = require('./'+ config.env);
  envConfig = envConfig|| {};
 } catch(e) {
    envConfig = {};
 }
 
 module.exports = _.merge(config, envConfig);
 