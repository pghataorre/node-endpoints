const mongoose = require('mongoose');
const { teamsModel, managersModel } = require('../models/models');
const config = require('../config');
const { singleLeagueTeams, leagueTeams } = require('./leagueTeams');
mongoose.connect(config.mongooseUrl, config.mongooseParams);

const getTeamsList = async (req, res) => {
  try {
    // Use of populate('looks the actual Teams property key manager - the value come from the Schema content');
    const teamsCollection = await teamsModel.find({}).populate('manager');

    res.status(200).json({
      ...leagueTeams,
      data: teamsCollection,
    });

  } catch(error) {
    res.status(500).json({...error});
  }
}

const getSingleTeams = async (req, res) => {
  try {
    const singleTeam = await teamsModel.findById(req.params.id).populate('manager');
    res.status(200).json({
      ...singleLeagueTeams,
      data: singleTeam
    });

  } catch(error) {
    res.status(500).json({...error});
  }
}


const createTeam = async (req, res) => {
  try {
    const modelResponse = await teamsModel.create(req.body);
    res.status(201).json(modelResponse);
  } catch(error) {
    res.status(500).json(error);
  }
}

const deleteSingleTeam = async (req, res) => {
  try {
    const deleteResponse = await teamsModel.deleteOne({_id: req.params.id});
    res.status(200).json(deleteResponse);
  } catch (error) {
    res.status(500).json(error);
  }
}

module.exports = { 
  getTeamsList,
  getSingleTeams,
  createTeam,
  deleteSingleTeam
};
