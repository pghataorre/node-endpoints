const express = require('express');
const router = express.Router();
const checkAuth = require('../common/ checkAuth');
const { 
  getTeamsList,
  getSingleTeams,
  deleteSingleTeam,
  createTeam } = require('./teamsController');

router.get('/', async (req, res, next) => {
  await getTeamsList(req, res);
});

router.get('/:id', async (req, res, next) => {
  await getSingleTeams(req, res);
});

router.post('/',checkAuth, async (req, res, next) => {
  await createTeam(req, res);
});

router.delete('/:id',checkAuth, async (req, res, next) => {
  await deleteSingleTeam(req, res);
});

module.exports = router;
