const leagueTeams = {
  "desc": "FA Premier League Teams",
  "response": "200 application/json",
  "data": [
    {
      "id": 1,
      "name": "Arsenal FC",
      "teamBadge": 'arsenal',
    },
    {
      "id": 2,
      "name": "Aston Villa FC",
      "teamBadge": 'astonvilla',
    }, 
    {
      "id": 3,
      "name": "Brighton & Hove Albion FC",
      "teamBadge": 'brighton',
    },
    {
      "id": 4,
      "name": "Burnley FC",
      "teamBadge": 'burnley',
    },  
    {
      "id": 5,
      "name": "Chelsea FC",
      "teamBadge": 'chelsea',
    },
    {
      "id": 6,
      "name": "Crystal Palace Fc",
      "teamBadge": 'crystal-palace',
    },
    {
      "id": 7,
      "name": "Everton",
      "teamBadge": 'everton',
    },
    {
      "id": 8,
      "name": "Fullham FC",
      "teamBadge": 'fullham',
    },
    {
      "id": 9,
      "name": "Leeds United",
      "teamBadge": 'leeds',
    },
    {
      "id": 10,
      "name": "Leicester City FC",
      "teamBadge": 'leicester',
    },
    {
      "id": 11,
      "name": "Liverpool FC",
      "teamBadge": 'liverpool',
    },  
    {
      "id": 12,
      "name": "Manchester City",
      "teamBadge": 'manchestercity',
    },
    {
      "id": 13,
      "name": "Manchester Utd",
      "teamBadge": 'manchesterunited',
    },
    {
      "id": 14,
      "name": "Newcastle United",
      "teamBadge": 'newcastle',
    },
    {
      "id": 15,
      "name": "Sheffield Utd",
      "teamBadge": 'sheffieldutd',
    },
    {
      "id": 16,
      "name": "Southampton FC",
      "teamBadge": 'southampton',
    },
    {
      "id": 17,
      "name": "West Bromich Albion",
      "teamBadge": 'wba',
    },
    {
      "id": 18,
      "name": "West Ham Utd",
      "teamBadge": 'westham',
    },  
    {
      "id": 19,
      "name": "Wolverhampton",
      "teamBadge": 'wolverhampton',
    },
    {
      "id": 20,
      "name": "Wankers",
      "teamBadge": 'wankers',
    },    
  ]
};

const singleLeagueTeams = {
  "desc": "Single league team details",
  "response": "200 application/json",
  "data": {
      "id": 0,
      "name": '',
      "teamBadge": '',
    },
};

const singleLeagueTeamDelete = {
  "desc": "Single team deleted",
  "response": "200 application/json",
  "teamDeleted": {}
};

module.exports = {
  leagueTeams,
  singleLeagueTeams,
  singleLeagueTeamDelete
};

