const express = require('express');
const logger = require('morgan');
const fs = require('fs');
const path = require('path')
const app = express(); 

const config = require('./config');
const teamsRouter = require('./teams/teamsRoute');
const tournamentsRouter = require('./tournaments/tournamentsRoute');

const createManagerRoute = require('./manager/createManagerRoute');
const getPlayersListRoute = require('./players/getPlayersListRoute');
const getSinglePlayersRoute = require('./players/getSinglePlayerRoute');
const deleteSinglePlayerRoute = require('./players/deleteSinglePlayerRoute');
const usersRoute = require('./users/usersRoute');

const { returnResponseMessage } = require('./common/common');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.param('id', (req, res, next, id) => {
  if (!id) {
    return returnResponseMessage(req, res, 404, 'Record ID not correct');
  } else {
    req.id = id;
    next();
  }
});

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, HEAD, DELETE');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, content-type, Accept');
  next();
});

app.get('/', (req, res) => {
  fs.readFile('index.html', (err, buffer) => {
    const html = buffer.toString();
    res.setHeader('Content-Type', 'text/html')
    res.send(html);
  });
});

// SIGN IN REGISTER
// ------------------------------------
app.use('/users', usersRoute);

// PLAYERS ROUTES
// ------------------------------------
app.get('/teamplayers/:id', getPlayersListRoute);
app.get('/player/:id', getSinglePlayersRoute);
app.delete('/player/delete/:id', deleteSinglePlayerRoute);

// TOURNAMENTS ROUTES
// ------------------------------------
app.use('/tournaments', tournamentsRouter);

// TEAMS ROUTES
// ------------------------------------
app.use('/teams', teamsRouter);

// MANAGER ROUTES
// ------------------------------------
app.post('/manager/create', createManagerRoute);

app.use((err, req, res, next) => {
  if (err) {
    return returnResponseMessage(req, res, 400, err.message);
  }
});

const port = config.port;

app.listen(port, () => {
  console.log('\n************************************************************\n Listening on port: ', port, '\n************************************************************\n');
});

module.exports = app;
